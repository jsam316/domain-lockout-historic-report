domain-lockout-historic-report
===================

Get's any locked out user event from the last number of days

**Summary:**

- This script runs with no parameters!
- It will go through an array of domain controllers, search the logs, find account lockout events, and add them to an array.
- It will then parse each event in the array and pull out the relevant information to be displayed.
- The information is then written to a csv file

**Changelog:**

- Version .02 (September 20, 2014)
	- Functionalized everything!
	- Added a send email function to notify if desired
	- Output to CSV and sending email is now controlled by variable to make it easier
	- NOTE: Path to email templates will need to be changed by whoever uses the script.
- Version .01 (September 18, 2014)
	- Initial Version
	- Minimal error checking, but it works!